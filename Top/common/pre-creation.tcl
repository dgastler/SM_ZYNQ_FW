set path [file normalize "[file normalize [file dirname [info script]]]"]
source ${path}/BU_settings.tcl

# clean up the autogen path
file delete -force -- ${apollo_root_path}/configs/${build_name}/autogen
file delete ${apollo_root_path}/kernel/config_${build_name}.yaml

cd $apollo_root_path

set PYTHONPATH $::env(PYTHONPATH)
set PYTHONHOME $::env(PYTHONHOME)
set STORED_PATH $::env(PATH)
set STORED_LD_LIBRARY_PATH $::env(LD_LIBRARY_PATH)
puts "################ env(PATH) ################"
set ::env(PATH) "/usr/bin" 

puts "################ LD_LIBRARY_PATH ################"
set ::env(LD_LIBRARY_PATH) ""
puts $::env(LD_LIBRARY_PATH) 


puts "################ PYTHONPATH ################"
unset -nocomplain ::env(PYTHONPATH)

puts "################ PYTHONHOME ################"
unset -nocomplain ::env(PYTHONHOME)


puts [exec which python3]
puts "################ PYTHON3 ################"
puts [exec python3 -V]

puts "################ starting pre-build ################"
if {[catch {exec make prebuild_${build_name}} result] != 0} {
    puts $result
    error "Failed in prebuild"
} else {
    puts "prebuild script"
    foreach line [split $result \n] {
	puts "   > $line"
    }    
}

puts "################ ending pre-build ################"
puts $BUILD_SCRIPTS_PATH
  
set ::env(PYTHONPATH) $PYTHONPATH
set ::env(PYTHONHOME) $PYTHONHOME
set ::env(PATH) $STORED_PATH
set ::env(LD_LIBRARY_PATH) $STORED_LD_LIBRARY_PATH

cd $HOG_PATH      
puts "################ BUILD_SCRIPTS_PATH ################"
  
puts $BUILD_SCRIPTS_PATH


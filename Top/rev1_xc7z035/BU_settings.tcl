global apollo_root_path;   set apollo_root_path [file normalize "[file normalize [file dirname [info script]]]/../../"]
global BUILD_SCRIPTS_PATH; set BUILD_SCRIPTS_PATH $apollo_root_path/build-scripts/
set HOG_PATH $apollo_root_path/Hog/Tcl

global BD_PATH;            set BD_PATH $apollo_root_path/bd
global bd_name;            set bd_name zynq_bd 
global build_name;         set build_name rev1_xc7z035
global autogen_path;       set autogen_path configs/$build_name/autogen/
